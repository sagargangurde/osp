<!DOCTYPE html>
<html>
   <head>
      <title></title>
      <link href="static/css/animate.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
      <script src="static/js/jquery.min.js"></script>
      <script src="static/js/html2canvas.js"></script>
      <script src="static/js/html2canvas.min.js"></script>
      <script src="static/js/jquery.plugin.html2canvas.js"></script>
      <script src="static/js/jquery.lettering.js"></script>
      <script src="static/js/jquery.textillate.js"></script>
      <script type="text/javascript">

          var generate_images = [];
          var subs  = [];
          var sub_index = 0;
          var timestamp = Math.floor(Date.now() / 1000);
          var text_effects = ["bounce", "tada", "flash", "swing", "wobble", "hinge"];

          function send_base64_strings(sub_index, shots) {
              shots.pop();
              $.ajax({        
                 type: "POST",
                 url: "parsebase64.php",
                 data: { imgData : shots, subtitle_index : sub_index},
                 success: function() {
                      // alert('Image data has been sent to the server!');
                      if (sub_index == subs.length-1){
                          generateVdo();
                      }
                 }
              }); 
          }

          function stop_generating(sub_index){
            generate_images[sub_index] = 1;

          }

          function start_capturing_frames(){
            // alert(subs);
            capture_frames(subs[sub_index]);
          }

          function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
          }

          function capture_frames(sub){
            $('#subtitle').remove();
            $('#container').append("<h1 id='subtitle'></h1>");
            $('#subtitle').html(sub);

            var shots = [];
            generate_image(sub_index, shots);
            $('#subtitle').textillate({
                   selector: '.texts',
                   loop: false,
                   minDisplayTime: 1000,
                   initialDelay: 0,
                   in: {
                       effect: text_effects[getRandomInt(0,5)],
                       delayScale: 1.5,
                       delay: 50,
                       sync: false,
                       shuffle: false,
                       callback: function () {
                        // alert(sub_index);
                          stop_generating(sub_index);
                          generate_images[sub_index] = 1;
                          sub_index += 1
                          if(sub_index < subs.length){
                            capture_frames(subs[sub_index]);
                          }
                          else{
                            return false;
                          }
                       }
                   },
                   autoStart: true,
                   inEffects: [],
                   outEffects: [ 'hinge' ]
               });
          }

          function get_subs(){
            $.getJSON('parsesrt.php', {}, function(data) {
                $.each(data, function(index, element) {

                    subs.push(element.text);
                });

                start_capturing_frames();
            });

            
          }

          function generate_image(sub_index, shots) {

                  if(generate_images[sub_index]){
                    send_base64_strings(sub_index, shots);
                    return false;
                  } 

                  html2canvas($("#container"), {
                      onrendered: function(canvas) {
                          var img = canvas.toDataURL("image/png");
                          shots.push(img.replace("data:image/png;base64,",""));
                      },
                      height: 480,
                      width: 640,
                  });

                  setTimeout(generate_image,25, sub_index, shots);
          }

          $( document ).ready(function() {
             if($("#message").length > 0 && $("#message").html().indexOf('Uploaded successfully') != -1)
             {
                get_subs();
             } 
          });

          function generateVdo()
          {
            $.ajax({        
                 type: "POST",
                 url: "generatevdo.php",
                 data: {timestamp: timestamp},
                 success: function() {
                      // alert("video: " + timestamp + ".mp4 generated!")
                      // $('body').append('<video width="640" height="480" controls="controls"><source src="output/video.webm" type="video/webm"></source></video>')

                      // alert("Your video is now available!");
                      $('#parent_container').html('<video width="640" height="480" controls="controls"><source src="output/video.webm" type="video/webm"></source></video>');
                      alert("Your video is now available!");
                      
                 }
              });
          }


      </script>

      <style type="text/css">

      @keyframes animatedBackground {
        from { background-position: 0 0; }
        to { background-position: 100% 0; }
      }

      h1{
        color: black;
        margin-top: 200px;
        margin-left: 100px;
        font-family: 'Raleway';
        width: 440px;
      }
      div#container{
        height: 480px;
        width: 640px;
        /*background: none repeat scroll 0 0 white;*/
        overflow: auto;

        background-image: url(static/images/clouds.png);
        background-position: 0px 0px;
        /*background-repeat: repeat-x;*/
        /*background-size:cover;*/
        background-repeat: no-repeat;

        /*animation: animatedBackground 40s linear infinite;*/
      
      }
      div#parent_container{
        height: 485px;
        width: 645px;
        /*background: none repeat scroll 0 0 white;*/
        overflow: auto;
      }
      </style>

   </head>
   <body>
   <br/>
   <div id="parent_container">
     <div id="container">
        <h1 id="subtitle"></h1>
     </div>
   </div>
      <br/>
      <p style="margin:100px;">
  <?php 
    $msgMap = array('file-upload-success' =>'Files Uploaded successfully!');
    if(isset($_GET['msg']) && (trim($_GET['msg']) != '')) 
    { 
  ?>
    <span style="color:#00ff00;" id="message">
      <?php echo isset($msgMap[$_GET['msg']])?$msgMap[$_GET['msg']]:''; ?>
    </span><br/>
  <?php 
    } 
  ?>
  <a href="file_upload.php">Upload Files</a>
   </p>
   </body>
</html>