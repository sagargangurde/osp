"""
This module needs movie_py and its dependacies intalled.
This module is tested on ubuntu 12.04.
For more details about installation check: http://zulko.github.io/moviepy/install.html
"""
import os
import datetime
import time
import sys

import pysrt
from moviepy.editor import *

if __name__ == '__main__':
    # Input directory where frames are generated
    input_frames_dir = "input/frames/"

    # Input srt file path
    input_srt_file = 'input/subs.srt'

    # Input audio file path
    input_audio_file = "input/audio.mp3"

    # Output file path
    # output_file_path = 'output/'+str(time.time())+'.mp4'
    output_file_path = 'output/'+str(sys.argv[1])+'.mp4'

    subs = pysrt.open(input_srt_file)
    music = AudioFileClip(input_audio_file)
    subclips = []

    # Following logic could be used instead of the implemented one where we have to handle the time slice without a subtitle
    '''
    wait_time = subs[0].start
    sub_x_duration = subs[x].end - subs[x].start
    sub_x_wait = subs[x+1].start - subs[x].end 
    '''

    FRAME_RATE = 10
    total_subs = len(subs)
    
    for i in range(0, total_subs):
        j = i+1
        if j == 1:
            duration = str(subs[j].start)
        elif j!= total_subs:
            duration = str(subs[j].start - subs[i].start)
        else:
            duration = str(subs[i].end - subs[i].start)

        # Following logic could be used (with line 51) if different framerate is needed while generating subclips
        """delta = time.strptime(str(duration).split(',')[0],'%H:%M:%S')
        time_in_seconds = int(datetime.timedelta(hours=delta.tm_hour,minutes=delta.tm_min,seconds=delta.tm_sec).total_seconds())"""

        frames = [input_frames_dir + filename for filename in os.listdir(input_frames_dir) if filename.startswith("frame" + str(i) + "-")]
        frames.sort(cmp=None, key=None, reverse=False)

        #req_fps = len(frames)//time_in_seconds
        req_fps = FRAME_RATE
        clip = ImageSequenceClip(frames, fps=req_fps).set_duration(str(duration))
        subclips.append(clip)

    final_clip = concatenate_videoclips(subclips)
    audio = afx.audio_loop(music, duration=final_clip.duration)
    final_clip = final_clip.set_audio(audio)
    final_clip.write_videofile(output_file_path, fps=FRAME_RATE, codec='mpeg4')
