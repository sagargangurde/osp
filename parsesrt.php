<?php
define('SRT_STATE_SUBNUMBER', 0);
define('SRT_STATE_TIME',      1);
define('SRT_STATE_TEXT',      2);
define('SRT_STATE_BLANK',     3);

$lines   = file('input/subs.srt');

$subs    = array();
$state   = SRT_STATE_SUBNUMBER;
$subNum  = 0;
$subText = '';
$subTime = '';

foreach($lines as $line) {
    switch($state) {
        case SRT_STATE_SUBNUMBER:
            $subNum = trim($line);
            $state  = SRT_STATE_TIME;
            break;

        case SRT_STATE_TIME:
            $subTime = trim($line);
            $state   = SRT_STATE_TEXT;
            break;

        case SRT_STATE_TEXT:

            if (trim($line) == '') {
                $sub = new stdClass;
                $sub->number = $subNum;
                list($sub->startTime, $sub->stopTime) = explode(' --> ', $subTime);
                $sub->text   = $subText;
                $subText     = '';
                $state       = SRT_STATE_SUBNUMBER;

                $subs[]      = $sub;
            } else {
                $subText .= $line;
            }
            break;
    }
}


for($i = 0; $i < count($subs); $i++) {
	$dialog = $subs[$i]->text;
	$dialog = str_replace("<i>", "", $dialog);
	$dialog = str_replace("</i>", "", $dialog);
	$dialog = str_replace("<b>", "", $dialog);
	$dialog = str_replace("</b>", "", $dialog);
	$dialog = str_replace("<u>", "", $dialog);
	$dialog = str_replace("</u>", "", $dialog);
    $dialog = str_replace("\r\n", "", $dialog);
	$subs[$i]->text = $dialog;
}

echo stripslashes(json_encode($subs));

?>