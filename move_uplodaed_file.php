<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
$uploaddir = realpath(dirname(__FILE__)).'/input/';
$uploadfile = $uploaddir . 'audio.mp3'; //basename($_FILES['sound']['name']) 
$uploadfile_second = $uploaddir . 'subs.srt'; //basename($_FILES['subtitle']['name'])

$file_uploaded_1 = $_FILES['sound']['tmp_name'];
$file_uploaded_2 = $_FILES['subtitle']['tmp_name'];
$file_info = new finfo(FILEINFO_MIME);
$mime_type1 = (string)$file_info->buffer(file_get_contents($file_uploaded_1));  // e.g. gives "audio/mpeg"
$mime_type2 = (string)$file_info->buffer(file_get_contents($file_uploaded_2));  // e.g. gives "text/plain"

if(stripos($mime_type1,'audio') === False)
{
	header("Location:file_upload.php?error=audio_file_format_invalid");
	die;
}

if(stripos($mime_type2,'text/plain') === False)
{
	header("Location:file_upload.php?error=subtitle_file_format_invalid");
	die;
}


$error = '';
if (move_uploaded_file($file_uploaded_1, $uploadfile)) {
    echo "<br /><br />Sound file is valid, and was successfully uploaded.\n";
} else {
    $error = 'Path_error';
}


if (move_uploaded_file($file_uploaded_2, $uploadfile_second)) {
    echo "<br /><br />Sub title text file is valid, and was successfully uploaded.\n";
} else {
    $error = 'Path_error';
}

if(trim($error) == 'Path_error')
{
	header("Location:file_upload.php?error=file_path_invalid");
	die;
}

header("Location:index.php?msg=file-upload-success");
die;

?>

